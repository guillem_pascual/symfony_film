<?php

namespace CacheBundle\EventListener;

use CacheBundle\Services\CacheFilmDeleter;
use Symfony\Component\EventDispatcher\Event;

class FilmListener
{
    public function onFilmAdded(Event $event)
    {
        // You get the exception object from the received event
        $film = $event->getFilm();
        $message = sprintf(
            'Added film was: %s with name  %s ',
            $film->getId(),
            $film->getName()
        );

        $cacheFilmDeleter = new CacheFilmDeleter();
        $cacheFilmDeleter->execute();
        echo json_encode($message);
    }

    public function onFilmRemoved(Event $event)
    {
        // You get the exception object from the received event
        $film = $event->getFilm();
        $message = sprintf(
            'Removed film was: %s with name  %s ',
            $film->getId(),
            $film->getName()
        );
        $cacheFilmDeleter = new CacheFilmDeleter();
        $cacheFilmDeleter->execute();
        echo json_encode($message);
    }
    public function onFilmUpdated(Event $event)
    {
        // You get the exception object from the received event
        $film = $event->getFilm();
        $message = sprintf(
            'Edited film was: %s with name  %s ',
            $film->getId(),
            $film->getName()
        );
        $cacheFilmDeleter = new CacheFilmDeleter();
        $cacheFilmDeleter->execute();
        echo json_encode($message);
    }
}