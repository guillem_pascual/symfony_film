<?php

namespace CacheBundle\Services;

use AppBundle\Services\FilmLister;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

class CacheFilmLister
{
    private $filmLister;

    public function __construct(FilmLister $filmLister)
    {
        $this->filmLister = $filmLister;
    }

    public function execute()
    {
        $fs = new Filesystem();
        if($fs->exists('/tmp/filmList.json')){
            $allFilmsJSON = file_get_contents('/tmp/filmList.json');
            return $allFilmsJSON;
        }
        else {
            $allFilmsJSON =  $this->filmLister->execute();
            try {
                $fs->dumpFile('/tmp/filmList.json', $allFilmsJSON);
            }
            catch(IOException $e) {
            }
            return $allFilmsJSON;
        }
    }
}