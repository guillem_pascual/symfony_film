<?php

namespace CacheBundle\Services;

use Symfony\Component\Filesystem\Filesystem;

class CacheFilmDeleter
{
    public function __construct()
    {
    }

    public function execute()
    {
        $fs = new Filesystem();
        if($fs->exists('/tmp/filmList.json')){
            unlink('/tmp/filmList.json');
        }
    }
}