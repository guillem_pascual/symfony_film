<?php

namespace AppBundle\Services;

use AppBundle\Entity\Film;
use AppBundle\Event\FilmAddedEvent;
use DateTime;

class FilmAdder extends AbstractFilm
{
    public function execute($name,$year,$date,$imdb)
    {
        $film = new Film();
        $film->setName($name);
        $film->setYear(new DateTime($year));
        $film->setDate(new DateTime($date));
        $film->setImdb($imdb);
        $this->em->persist($film);
        $this->em->flush($film);

        $result = 'OK';

        $event = new FilmAddedEvent($film);
        $this->ed->dispatch(FilmAddedEvent::NAME, $event);

        return json_encode($result);
    }
}