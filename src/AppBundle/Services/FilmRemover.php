<?php

namespace AppBundle\Services;

use AppBundle\Entity\Film;
use AppBundle\Event\FilmRemovedEvent;
use Doctrine\ORM\EntityManager;

class FilmRemover extends AbstractFilm
{
    public function execute($id)
    {
        $film = $this->em->getRepository('AppBundle:Film')->find($id);
        $this->em->remove($film);
        $this->em->flush($film);

        $result = 'OK';

        $event = new FilmRemovedEvent($film);
        $this->ed->dispatch(FilmRemovedEvent::NAME, $event);

        return json_encode($result);
    }
}