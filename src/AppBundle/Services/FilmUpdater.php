<?php

namespace AppBundle\Services;

use AppBundle\Entity\Film;
use AppBundle\Event\FilmUpdatedEvent;
use DateTime;
use Doctrine\ORM\EntityManager;

class FilmUpdater extends AbstractFilm
{
    public function execute($id,$name,$year,$date,$imdb)
    {
        $film = $this->em->getRepository('AppBundle:Film')->find($id);

        $film->setName($name);
        $film->setYear(new DateTime($year));
        $film->setDate(new DateTime($date));
        $film->setImdb($imdb);

        $this->em->persist($film);
        $this->em->flush($film);

        $result = 'OK';

        $event = new FilmUpdatedEvent($film);
        $this->ed->dispatch(FilmUpdatedEvent::NAME, $event);

        return json_encode($result);
    }
}