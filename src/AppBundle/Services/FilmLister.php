<?php

namespace AppBundle\Services;

use AppBundle\Entity\Film;
use Doctrine\ORM\EntityManager;

class FilmLister extends AbstractFilm
{
    public function execute()
    {
        //$fname = "/tmp/filmList.csv";

        //die($fname);

        $allFilms = $this->em->getRepository('AppBundle:Film')->findAll();

        $result = array();
        foreach($allFilms as $film){
           $result[] = $film->toArray();
        }
        return json_encode($result);
    }
}