<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveFilmCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('demo:removefilm')
            ->setDescription('Remove Film')
            ->addArgument(
                'id',
                InputArgument::REQUIRED,
                'id'
            )
        ;
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $removeFilm = $this->getContainer()->get('filmremover');
        $id = $input->getArgument('id');

        $output->writeln($removeFilm->execute($id));
    }
}