<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListFilmCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('demo:listfilm')
            ->setDescription('List Films')
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'name'
            )
        ;
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $listFilm = $this->getContainer()->get('filmlister');
        $name = $input->getArgument('name'); // for the moment we do nothing with name

        $output->writeln($listFilm->execute());
    }
}