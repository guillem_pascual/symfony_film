<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class EditFilmCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('demo:editfilm')
            ->setDescription('Edit Film')
            ->addArgument(
                'id',
                InputArgument::REQUIRED,
                'id'
            )
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Name'
            )
            ->addArgument(
                'year',
                InputArgument::REQUIRED,
                'Year'
            )
            ->addArgument(
                'date',
                InputArgument::REQUIRED,
                'date'
            )
            ->addArgument(
                'imdb',
                InputArgument::REQUIRED,
                'ImDB'
            )
        ;
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $editFilm = $this->getContainer()->get('filmediter');

        $id = $input->getArgument('id');
        $name = $input->getArgument('name');
        $year = $input->getArgument('year');
        $date = $input->getArgument('date');
        $imdb = $input->getArgument('imdb');

        $output->writeln($editFilm->execute($id,$name,$year,$date,$imdb));
    }
}