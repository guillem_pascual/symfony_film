<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddFilmCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('demo:addfilm')
            ->setDescription('Add Film')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Name'
            )
            ->addArgument(
                'year',
                InputArgument::REQUIRED,
                'Year'
            )
            ->addArgument(
                'date',
                InputArgument::REQUIRED,
                'date'
            )
            ->addArgument(
                'imdb',
                InputArgument::REQUIRED,
                'ImDB'
            )
        ;
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $addFilm= $this->getContainer()->get('filmadder');

        $name = $input->getArgument('name');
        $year = $input->getArgument('year');
        $date = $input->getArgument('date');
        $imdb = $input->getArgument('imdb');

        $output->writeln($addFilm->execute($name,$year,$date,$imdb));
    }
}