<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class HelloCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('demo:calc')
            ->setDescription('Calculadora')
            ->addArgument(
                'a',
                InputArgument::REQUIRED,
                'Number to multiply?'
            )
            ->addArgument(
                'b',
                InputArgument::REQUIRED,
                'Number to multiply'
            )
        ;
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $calc = $this->getContainer()->get('Calc');

        $a = $input->getArgument('a');
        $b = $input->getArgument('b');

        $output->writeln($calc->mul($a,$b));
    }

}