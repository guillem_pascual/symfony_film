<?php

namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class FilmController extends Controller
{
    /**
     * @Route("/film/add/{name}/{year}/{date}/{imdb}", name="add")
     */
    public function adderAction($name,$year,$date,$imdb)
    {
        $filmAdder = $this->get('FilmAdder');
        return new Response($filmAdder->execute($name,$year,$date,$imdb));
    }

     /**
     * @Route("/film/del/{id}", name="del")
     */
    public function removerAction($id)
    {
        $filmDeleter = $this->get('FilmRemover');
        return new Response($filmDeleter->execute($id));
    }

    /**
     * @Route("/film/edit/{id}/{name}/{year}/{date}/{imdb}", name="edit")
     */
    public function updaterAction($id,$name,$year,$date,$imdb)
    {
        $filmUpdater = $this->get('FilmUpdater');
        return new Response($filmUpdater->execute($id,$name,$year,$date,$imdb));
    }

    /**
     * @Route("/film/list", name="list")
     */
    public function listerAction()
    {
        $filmLister = $this->get('FilmLister');
        return new Response($filmLister->execute());
    }
}
