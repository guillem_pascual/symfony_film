<?php

namespace AppBundle\Event;
use AppBundle\Entity\Film;
use Symfony\Component\EventDispatcher\Event;

/**
 * The order.placed event is dispatched each time an order is created
 * in the system.
 */
class FilmRemovedEvent extends Event
{
    const NAME = 'film.removed';

    protected $film;

    public function __construct(Film $film)
    {
        $this->film = $film;
    }

    public function getFilm()
    {
        return $this->film;
    }
}