<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_faaae31eb51cd500176d733ab6c839e353c4bd3b472a7988c3383a058b8f8b48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_47dfbad5831be269c8e12abc40f75443a1c0b71937c09e63d2f52c97d8d1f70e = $this->env->getExtension("native_profiler");
        $__internal_47dfbad5831be269c8e12abc40f75443a1c0b71937c09e63d2f52c97d8d1f70e->enter($__internal_47dfbad5831be269c8e12abc40f75443a1c0b71937c09e63d2f52c97d8d1f70e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_47dfbad5831be269c8e12abc40f75443a1c0b71937c09e63d2f52c97d8d1f70e->leave($__internal_47dfbad5831be269c8e12abc40f75443a1c0b71937c09e63d2f52c97d8d1f70e_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f727e320b568908a9b154f8ea7a6d5f028ee5fc9adec93dc5f0bd12929e518f7 = $this->env->getExtension("native_profiler");
        $__internal_f727e320b568908a9b154f8ea7a6d5f028ee5fc9adec93dc5f0bd12929e518f7->enter($__internal_f727e320b568908a9b154f8ea7a6d5f028ee5fc9adec93dc5f0bd12929e518f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_f727e320b568908a9b154f8ea7a6d5f028ee5fc9adec93dc5f0bd12929e518f7->leave($__internal_f727e320b568908a9b154f8ea7a6d5f028ee5fc9adec93dc5f0bd12929e518f7_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_bfe0b3288d921129f36363c27b34a02744b1940a5154aea54dd9ac9d5d849d57 = $this->env->getExtension("native_profiler");
        $__internal_bfe0b3288d921129f36363c27b34a02744b1940a5154aea54dd9ac9d5d849d57->enter($__internal_bfe0b3288d921129f36363c27b34a02744b1940a5154aea54dd9ac9d5d849d57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_bfe0b3288d921129f36363c27b34a02744b1940a5154aea54dd9ac9d5d849d57->leave($__internal_bfe0b3288d921129f36363c27b34a02744b1940a5154aea54dd9ac9d5d849d57_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_8b4a5d51b5b20d82027429613195c6a2f0951128af0a2078bc6438165c76629e = $this->env->getExtension("native_profiler");
        $__internal_8b4a5d51b5b20d82027429613195c6a2f0951128af0a2078bc6438165c76629e->enter($__internal_8b4a5d51b5b20d82027429613195c6a2f0951128af0a2078bc6438165c76629e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_8b4a5d51b5b20d82027429613195c6a2f0951128af0a2078bc6438165c76629e->leave($__internal_8b4a5d51b5b20d82027429613195c6a2f0951128af0a2078bc6438165c76629e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
