<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_6476c3e8b6c78f15ab91e9d35588cbefa81a01c563fdc59e595cea4cb1efeea3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28e3da4c8fcd8a4000a1c4021d96e2421793c5a8bb65770a3792e0c1aa7f389d = $this->env->getExtension("native_profiler");
        $__internal_28e3da4c8fcd8a4000a1c4021d96e2421793c5a8bb65770a3792e0c1aa7f389d->enter($__internal_28e3da4c8fcd8a4000a1c4021d96e2421793c5a8bb65770a3792e0c1aa7f389d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_28e3da4c8fcd8a4000a1c4021d96e2421793c5a8bb65770a3792e0c1aa7f389d->leave($__internal_28e3da4c8fcd8a4000a1c4021d96e2421793c5a8bb65770a3792e0c1aa7f389d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_74f3cf5479c8de5d344f681b641169f6afc9f28aaf6ec04997966eadde4225a7 = $this->env->getExtension("native_profiler");
        $__internal_74f3cf5479c8de5d344f681b641169f6afc9f28aaf6ec04997966eadde4225a7->enter($__internal_74f3cf5479c8de5d344f681b641169f6afc9f28aaf6ec04997966eadde4225a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_74f3cf5479c8de5d344f681b641169f6afc9f28aaf6ec04997966eadde4225a7->leave($__internal_74f3cf5479c8de5d344f681b641169f6afc9f28aaf6ec04997966eadde4225a7_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_71ba9fc8a87505b3c90a9b13a9afe36d8a459712b59f38a771ad5280af350b8e = $this->env->getExtension("native_profiler");
        $__internal_71ba9fc8a87505b3c90a9b13a9afe36d8a459712b59f38a771ad5280af350b8e->enter($__internal_71ba9fc8a87505b3c90a9b13a9afe36d8a459712b59f38a771ad5280af350b8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_71ba9fc8a87505b3c90a9b13a9afe36d8a459712b59f38a771ad5280af350b8e->leave($__internal_71ba9fc8a87505b3c90a9b13a9afe36d8a459712b59f38a771ad5280af350b8e_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_de9adcd17cf9ac4972ee0acc44cf7e124e1312db85da5658ecae99e02bd0d930 = $this->env->getExtension("native_profiler");
        $__internal_de9adcd17cf9ac4972ee0acc44cf7e124e1312db85da5658ecae99e02bd0d930->enter($__internal_de9adcd17cf9ac4972ee0acc44cf7e124e1312db85da5658ecae99e02bd0d930_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_de9adcd17cf9ac4972ee0acc44cf7e124e1312db85da5658ecae99e02bd0d930->leave($__internal_de9adcd17cf9ac4972ee0acc44cf7e124e1312db85da5658ecae99e02bd0d930_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
