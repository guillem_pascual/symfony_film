<?php

/* base.html.twig */
class __TwigTemplate_f22e0e7fa596a2f527c3271485946516f58c3811f22aec381902afefef12c59c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67bd1109d823bc2db9047bc663dde048b951a49a5c79faf44a59ebb92d87a058 = $this->env->getExtension("native_profiler");
        $__internal_67bd1109d823bc2db9047bc663dde048b951a49a5c79faf44a59ebb92d87a058->enter($__internal_67bd1109d823bc2db9047bc663dde048b951a49a5c79faf44a59ebb92d87a058_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_67bd1109d823bc2db9047bc663dde048b951a49a5c79faf44a59ebb92d87a058->leave($__internal_67bd1109d823bc2db9047bc663dde048b951a49a5c79faf44a59ebb92d87a058_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b687462f49d56a4c5b2a5dadd2c581afae60e6e482383fe80989caee146bf84b = $this->env->getExtension("native_profiler");
        $__internal_b687462f49d56a4c5b2a5dadd2c581afae60e6e482383fe80989caee146bf84b->enter($__internal_b687462f49d56a4c5b2a5dadd2c581afae60e6e482383fe80989caee146bf84b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_b687462f49d56a4c5b2a5dadd2c581afae60e6e482383fe80989caee146bf84b->leave($__internal_b687462f49d56a4c5b2a5dadd2c581afae60e6e482383fe80989caee146bf84b_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_fac16cccc971b7405e0798489fa954270a673aa49149a475c41bf0123b432e52 = $this->env->getExtension("native_profiler");
        $__internal_fac16cccc971b7405e0798489fa954270a673aa49149a475c41bf0123b432e52->enter($__internal_fac16cccc971b7405e0798489fa954270a673aa49149a475c41bf0123b432e52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_fac16cccc971b7405e0798489fa954270a673aa49149a475c41bf0123b432e52->leave($__internal_fac16cccc971b7405e0798489fa954270a673aa49149a475c41bf0123b432e52_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_a107ebd651ae92e34dd0bea6bb38f45d801e1d750efe7f18a3e54a6580bb091c = $this->env->getExtension("native_profiler");
        $__internal_a107ebd651ae92e34dd0bea6bb38f45d801e1d750efe7f18a3e54a6580bb091c->enter($__internal_a107ebd651ae92e34dd0bea6bb38f45d801e1d750efe7f18a3e54a6580bb091c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_a107ebd651ae92e34dd0bea6bb38f45d801e1d750efe7f18a3e54a6580bb091c->leave($__internal_a107ebd651ae92e34dd0bea6bb38f45d801e1d750efe7f18a3e54a6580bb091c_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_86b0379369e6149af9932d2abb0c61f0ef4f358ba551622a9277025ac1ca7aca = $this->env->getExtension("native_profiler");
        $__internal_86b0379369e6149af9932d2abb0c61f0ef4f358ba551622a9277025ac1ca7aca->enter($__internal_86b0379369e6149af9932d2abb0c61f0ef4f358ba551622a9277025ac1ca7aca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_86b0379369e6149af9932d2abb0c61f0ef4f358ba551622a9277025ac1ca7aca->leave($__internal_86b0379369e6149af9932d2abb0c61f0ef4f358ba551622a9277025ac1ca7aca_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
